var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
    entry: {
        vendor: [
            'angular',
            'lodash',
            'angular-sanitize',
            'angular-ui-router',
            'angular-animate',
            './app/ionic/ionic.min.js',
            './app/ionic/ionic-angular.min.js',
            'firebase',
            'angularfire'
        ],
        app: './app/app'
    },
    output: {
        path: 'www/dist',
        filename: '[name].js'
    },
    devtool: 'source-map',
    resolve: {
        alias: {
            templates: path.join(__dirname, 'templates')
        }
    },
    module: {
        loaders: [
            { test: /\.html$/, loader: 'html' },
            { test: /\.json$/, loader: 'json' },
            { test: /\.css$/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader') },
            { test: /\.(jpg|png|gif|ttf|eot|svg|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
			{ test: /\.scss$/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader') }
        ]
    },
    plugins: [
        new ExtractTextPlugin('[name].css'),
		new BrowserSyncPlugin({
            host: 'localhost',
            port: 3000,
            server: { baseDir: ['www']}
        })
    ]
};
