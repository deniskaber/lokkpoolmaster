var controllers = angular.module('poolMasterApp.controllers', []);

require('./start.controller.js');
require('./pool.controller.js');
require('./pools.controller.js');

module.exports = controllers;