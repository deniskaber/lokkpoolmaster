angular.module('poolMasterApp.controllers')
    .controller('PoolCtrl', ['$rootScope', '$scope', '$state', '$stateParams', '$timeout', '$ionicLoading', '$ionicHistory', 'poolsService', 'sessionService', 'storageService', 'friendsService',
        function ($rootScope, $scope, $state, $stateParams, $timeout, $ionicLoading, $ionicHistory, poolsService, sessionService, storageService, friendsService) {
            var vm = this;

            var user = sessionService.getUserInfo();

            vm.states = require('./../data/states').states;

            vm.lotteries = [];
            vm.lotteriesMap = {};
            vm.poolObj = {};

            vm.friendsToInvite = {};

            vm.getLotteries = function () {
                return poolsService.getLotteries().then(function (lotteries) {
                    vm.lotteriesMap = lotteries;

                    var lotteriesArr = [];

                    angular.forEach(lotteries, function(value) {
                        lotteriesArr.push(value);
                    });

                    $timeout(function () {
                        vm.lotteries = _.sortBy(lotteriesArr, 'name');
                    });
                });
            };

            vm.getLotteries();

            function initNewPoolObj() {
                vm.poolObj = {
                    lotteries: {},
                    ticketsSettings: {},
                    moneySettings: {},
                    leaderId: user.id,
                    leader: {
                        imageUrl: user.imageUrl,
                        name: user.name
                    },
                    state: 'All'
                };
            }

            if ($stateParams.poolId) {
                poolsService.getPoolById($stateParams.poolId).then(function (pool) {
                    $timeout(function () {
                        vm.poolObj = pool;
                    });
                });
            } else {
                initNewPoolObj();
            }

            $scope.$watch('pool.poolObj.ticketsSettings.ask', function (value) {
                if (!value) {
                    vm.poolObj.moneySettings = {};
                    delete vm.poolObj.paypal;
                }
            });

            $scope.$watch('pool.poolObj.moneySettings.paypal', function (value) {
                if (!value) {
                    delete vm.poolObj.paypal;
                }
            });

            $scope.$watch('pool.poolObj.state', function (state) {
                if (state && (''+state).toLowerCase() != 'all') {
                    if (vm.hasLotteries()) {
                        var lotteriesToDelete = [];

                        _.forIn(vm.poolObj.lotteries, function (value, key) {
                            if (vm.lotteriesMap[key].state != state && vm.lotteriesMap[key].state.toLowerCase() != 'all') {
                                lotteriesToDelete.push(key);
                            }
                        });

                        _.each(lotteriesToDelete, function (key) {
                            delete vm.poolObj.lotteries[key];
                        });
                    }
                }
            });

            // vm.poolObj = {
            //     title: 'Super jackpotters',
            //     description: 'California lottery pool for anyone keen on playing and increasing the odds of winning',
            //     leaderId: "3riMeQ65biccELqEFihH7RAUGZC3",
            //     leader: {
            //         imageUrl: '',
            //         name: 'Adam Gray'
            //     },
            //     lotteries: {
            //         '-KPON2Qhk44Lyt_SDcGh': true,
            //         '-KPOOPhpxQryPlSueTu9': true,
            //         '-KPOOQZvs60_Du__TBdt': true
            //     },
            //     ticketsSettings: {
            //         own: true,
            //         ask: true
            //     },
            //     moneySettings: {
            //         paypal: true
            //     },
            //     paypal: 'paypal.me/adamjones',
            //     rules: 'Photo copy of your signature is required to add a ticket to the game.↵After adding your ticket to the pool, you cannot use it in other pools. If you are noticed using a ticket in other pool, you are automatically disqualified from the game.'
            // };

            vm.hasLotteries = function () {
                return _.some(_.values(vm.poolObj.lotteries));
            };

            vm.hasTicketsSettings = function () {
                return _.some(_.values(vm.poolObj.ticketsSettings));
            };

            vm.hasMoneySettings = function () {
                if (!_.some(_.values(vm.poolObj.moneySettings))) {
                    return false;
                }

                // if (vm.poolObj.moneySettings.paypal && !vm.poolObj.paypalUrl) {
                //     return false;
                // }

                return true;
            };

            vm.getInvitedFriends = function () {
                return _.reduce(vm.friendsToInvite, function (memo, value, key) {
                    if (value) {
                        memo.push(key);
                    }

                    return memo;
                }, []);
            };


            vm.setPoolType = function (type) {
                vm.poolObj.isPrivate = !!type;
                if (vm.isEdit) {
                    $rootScope.navigate('pool.review', null, null, {edited: true});
                } else {
                    $state.go('pool.description');
                }
            };

            vm.getPoolLotteriesNames = function () {
                var lotteries = _.reduce(vm.poolObj.lotteries, function (memo, item, key) {
                    if (!item) return memo;

                    if (vm.lotteriesMap[key]) {
                        memo.push(vm.lotteriesMap[key].name);
                    }

                    return memo;
                }, []);

                return lotteries.join(' / ');
            };

            vm.getPoolParticipation = function () {
                var types = _.reduce(vm.poolObj.ticketsSettings, function (memo, value, key) {
                    var text;

                    if (value) {
                        switch (key) {
                            case 'own':
                                text = 'Add a ticket';
                                break;
                            case 'ask':
                                text = 'Send money';
                                break;
                        }

                        memo.push(text);
                    }

                    return memo;
                },[]);

                return types.join(' / ');
            };

            vm.savePool = function () {
                return poolsService.createPool(vm.poolObj).then(function () {
                    $state.go('pool.invite');
                });
            };

            vm.skipInvite = function () {
                $ionicHistory.nextViewOptions({
                    disableBack: true,
                    historyRoot: true
                });

                //todo: add special logic when "from app" flag is on
                $state.go('pools');
            };

            vm.addImage = function () {
                document.getElementById('pool-image-file-input').click();
            };

            vm.onFileInputChange = function(input) {
                var file;

                if (!input.files || !input.files[0]) {
                    return;
                }

                file = input.files[0];

                $ionicLoading.show({
                    template: 'Loading...'
                });

                return storageService.addNewImage(file).then(function (url) {
                    $ionicLoading.hide();

                    $timeout(function() {
                        vm.poolObj.imageUrl = url;
                    });
                });
            };

            function getFriends() {
                return friendsService.getFriends().then(function (friends) {
                    vm.friendsMap = _.reduce(friends, function(memo, item) {
                        memo[item.id] = item;
                        return memo;
                    },{});

                    $timeout(function () {
                        vm.friends = friends;
                    });
                });
            }

            if ($rootScope.userId) {
                getFriends();
            } else {
                var off = $rootScope.$on('authDone', function () {
                    getFriends();
                    off();
                });
            }

            vm.inviteFriends = function() {
                var aFriendsToInvite = vm.getInvitedFriends();

                var phones = _.map(aFriendsToInvite, function(id) {
                    return vm.friendsMap[id].phoneNumber;
                });

                var href = document.createElement('a');

                href.href = 'sms:/open?addresses='+phones.join(',')+';body='+encodeURIComponent('My test msg');

                href.click();
            };

            vm.lotteriesSearch = function(lottery) {
                return (lottery.state == vm.poolObj.state) || (lottery.state.toLowerCase() == 'all');
            };

            vm.lotteriesSort = function(lottery) {
                return lottery.state.toLowerCase() == 'all' ? -1 : 1;
            };

            $scope.$on('$ionicView.beforeEnter', function (event, data) {
                if (data.stateName == 'pool.review') {
                    if (data.direction == "back" && !_.get($rootScope, 'specialFlags.edited')) {
                        if (vm.poolCopy) {
                            vm.poolObj = vm.poolCopy;
                        }
                    }
                } else {
                    vm.stateParams = data.stateParams;
                    vm.isEdit = !!(vm.stateParams && vm.stateParams.edit);
                    if (vm.isEdit) {
                        vm.poolCopy = _.cloneDeep(vm.poolObj);
                    } else {
                        vm.poolCopy = null;
                    }
                }
            });

            $scope.$on('$ionicView.afterEnter', function () {
                $scope.$broadcast('viewLoaded');
            });
        }
    ]);