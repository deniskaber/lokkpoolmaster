angular.module('poolMasterApp.controllers')
    .controller('StartCtrl', ['$scope', '$state', '$stateParams', '$ionicPopup', 'sessionService',
        function ($scope, $state, $stateParams, $ionicPopup, sessionService) {
            var vm = this;

            vm.user = _.assign({}, sessionService.getUserInfo());

            if ($stateParams.userkey) {
                console.dir($stateParams);
                sessionService.authByToken($stateParams.userkey).then(function() {
                    switch($stateParams.action) {
                        case 'newpool':
                            $state.go('pool.name');
                            break;
                        case 'editpool':
                            console.log($stateParams.userkey.poolId);
                            //todo: add param existence check
                            
                            $state.go('pool.review', {poolId: $stateParams.userkey.poolId});
                            break;
                        default:
                            $state.go('pools', {});
                            break;
                    }
                });
            }

            vm.loginUser = function (form) {
                if (!form.$valid) return;

                sessionService.loginUser(vm.user).then(function () {
                    $state.go('pools');
                }, function (errorMsg) {
                    $ionicPopup.alert({
                        title: 'Error',
                        template: errorMsg || 'Unknown error'
                    });
                });

            };
        }
    ]);