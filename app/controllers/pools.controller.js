angular.module('poolMasterApp.controllers')
    .controller('PoolsCtrl', ['$rootScope', '$scope', '$state', '$timeout', 'poolsService', 'sessionService',
        function ($rootScope, $scope, $state, $timeout, poolsService, sessionService) {
            var vm = this;

            var user = sessionService.getUserInfo();

            var page = 1,
                itemsPerPage = poolsService.getItemsPerPage();

            vm.search = '';
            vm.isSearchVisibly = false;
            vm.poolsList = [];
            vm.moreDataCanBeLoaded = true;

            var filterTextTimeout;

            vm.createPool = function() {
                $state.go('pool.name');
            };

            vm.doRefresh = function () {
                page = 1;
                vm.moreDataCanBeLoaded = true;
                getPoolsList(page).then(function () {
                    $scope.$broadcast('scroll.refreshComplete');
                }, function () {
                    $scope.$broadcast('scroll.refreshComplete');
                });
            };

            vm.loadMore = function () {
                page += 1;
                getPoolsList(page, vm.search).then(function () {
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }, function () {
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                });
            };

            $scope.$on("$ionicView.enter", function (event, data) {
                page = 1;
                vm.moreDataCanBeLoaded = true;

                if ($rootScope.userId) {
                    return getPoolsList(page);
                } else {
                    var off = $rootScope.$on('authDone', function() {
                        off();
                        return getPoolsList(page);
                    });
                }
            });

            function getPoolsList(page) {
                return poolsService.getAllUserPools(page).then(function (pools) {
                    $timeout(function () {
                        vm.poolsList = pools;
                    });

                    if (pools.length < page * itemsPerPage) {
                        vm.moreDataCanBeLoaded = false;
                    }
                });
            }

        }
    ]);