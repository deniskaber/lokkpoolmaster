angular.module('poolMasterApp.services')

    .factory('sessionService', ['$rootScope', '$http', '$state', '$firebaseAuth',
        function ($rootScope, $http, $state, $firebaseAuth) {
            var user = {};
            var code;
            var dbUsers;

            var authObj = $firebaseAuth();
            var firebaseUser;

            authObj.$onAuthStateChanged(function(firebaseUser) {
                if (firebaseUser) {
                    console.dir(firebaseUser);
                    console.log("Signed in as:", firebaseUser.uid);

                    firebaseUser = authObj.$getAuth();

                    dbUsers = firebase.database().ref("users");
                    dbUsers.child(firebaseUser.uid).once('value').then(function (snapshot) {
                        var user_data = snapshot.val();

                        if (!user_data) {
                            debugger;
                            // createNewUser();
                            // return;
                        }

                        user = {
                            id: firebaseUser.uid,
                            friends: user_data.friends || [],
                            balance: user_data.balance,
                            email: user_data.email,
                            facebookProfileId: user_data.facebookProfileId,
                            imageUrl: user_data.imageUrl,
                            name: user_data.name,
                            phoneNumber: user_data.phoneNumber,
                            pools: user_data.pools || []
                        };

                        //_saveUserInfo(true);
                        console.log("auth complete.");
                        //$state.go('menu.pools.open');

                        $rootScope.userId = user.id;
                        $rootScope.$broadcast('authDone');
                    });
                } else {
                    console.log("Signed out");
                    user = createNewUser();
                    //$state.go('start');
                    delete $rootScope.userId;
                }
            });

            function createNewUser() {
                console.log('createNewUser');
                user = {};
                authObj.$signOut();
                localStorage.clear();
                return user;
            }

            function getUserInfo() {
                return user;
            }

            function setUserInfo(newData) {
                for (var key in newData) {
                    if (newData.hasOwnProperty(key)) {
                        user[key] = newData[key];
                    }
                }

                return _saveUserInfo();
            }

            function loginUser(user) {
                if (!user.email || !user.password) return $q.reject();

                return authObj.$signInWithEmailAndPassword(user.email, user.password).catch(function (error) {
                    throw(error.message || 'Unknown error');
                });
            }

            function _saveUserInfo(onlyLocalStorage) {
                var fbuser = _.cloneDeep(user);
                _.each(user, function (value, key) {
                    if (value === undefined) {
                        delete fbuser[key];
                    }
                });

                localStorage.user = JSON.stringify(fbuser);

                if (!onlyLocalStorage) {
                    return dbUsers.child(user.id).update(fbuser);
                }

            }

            function joinPool(poolId) {
                if (!user.pools) {
                    user.pools = [];
                }
                user.pools.push(poolId);
                _saveUserInfo();
            }

            function authByToken(token) {
                return firebase.database().ref("users").orderByChild('secret_key').equalTo(token).once('value')
                    .then(function (snapshot) {
                        var data = snapshot.val();

                        if (!data) {
                            return $q.reject();
                        }

                        var user = _.values(data)[0];

                        return loginUser({
                            email: user.email,
                            password: user.password
                        });
                    });
            }

            return {
                getUserInfo: getUserInfo,
                setUserInfo: setUserInfo,
                createNewUser: createNewUser,
                loginUser: loginUser,
                joinPool: joinPool,
                authByToken: authByToken
            };
    }]);
