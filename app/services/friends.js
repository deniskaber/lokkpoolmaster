angular.module('poolMasterApp.services')

    .factory('friendsService', ['$rootScope', '$q', '$ionicPopup', 'sessionService',
        function ($rootScope, $q, $ionicPopup, sessionService) {
            var dbUsers = firebase.database().ref("users");
            
            var friends;
            
            function prepareFriends() {
                var userFriendsIds = sessionService.getUserInfo().friends;
                
                var requests = _.map(userFriendsIds, function(value, key) {
                    return dbUsers.child(key).once('value').then(function(snapshot) {
                        var uresObj = snapshot.val();

                        return {
                            id: uresObj.id,
                            name:  uresObj.name,
                            imageUrl: uresObj.imageUrl,
                            phoneNumber: uresObj.phoneNumber
                        };
                    })
                });
                
                return $q.all(requests);
            }
            
            function getFriends() {
                if (friends) {
                    return $q.resolve(friends);
                } else {
                    return prepareFriends().then(function(results) {
                        friends = results;
                    }).then(function() {
                        return $q.resolve(friends);
                    });
                }
            }
            
            return {
                getFriends: getFriends
            };
        }
    ]);
