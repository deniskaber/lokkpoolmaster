angular.module('poolMasterApp.services')

    .factory('poolsService', ['$q', '$timeout', '$firebaseObject', '$firebaseArray', 'sessionService',
        function ($q, $timeout, $firebaseObject, $firebaseArray, sessionService) {
            var dbPools = firebase.database().ref("pools");
            var gamesLoaded = false;

            var lotteries = $firebaseObject(firebase.database().ref("lotteries"));

            var poolsSnapshot,
                itemsPerPage = 20;

            function prepearePools(snapshot, page) {
                var deffered = $q.defer();

                if (gamesLoaded) {
                    deffered.resolve(prepearePoolsInner(snapshot, page));
                } else {
                    lotteries.$loaded().then(function () {
                        gamesLoaded = true;
                        deffered.resolve(prepearePoolsInner(snapshot, page));
                    });
                }

                return deffered.promise;
            }
            
            function prepearePool(pool, snapshot) {
                var nextDraw;

                pool.membersCount = snapshot.child('members').numChildren();
                pool.lotteryNames = _.chain(pool.lotteries).map(function (value, lottery) {
                    var poolLottery = lotteries[lottery];
                    
                    if (!poolLottery) {
                        return;
                    }

                    var nextDate = new Date(poolLottery.next_result_date);
                    
                    if (!nextDraw) {
                        nextDraw = nextDate && nextDate.getTime();
                    } else {
                        if (nextDate && nextDate.getTime() < nextDraw) {
                            nextDraw = nextDate.getTime();
                        }
                    }

                    return poolLottery.name;
                }).compact().join(', ').value();

                pool.nextDraw = new Date(nextDraw);
                
                return pool;
            }

            function prepearePoolsInner(snapshot, page) {
                poolsSnapshot = snapshot;

                var pools = _.chain(snapshot.val()).values().sortBy('name').value();

                _.each(pools, function (pool) {
                    return prepearePool(pool, snapshot.child(pool.id));
                });

                return pools;
            }

            var poolsService = {
                getAllUserPools: function (page, searchStr) {
                    var user = sessionService.getUserInfo();
                    return dbPools.orderByChild('leaderId').equalTo(user.id).once('value').then(function (snapshot) {
                        return prepearePools(snapshot, page);
                    });
                },

                getItemsPerPage: function () {
                    return itemsPerPage;
                },
                
                getPoolById: function(id) {
                    return dbPools.child(id).once('value').then(function (snapshot) {
                        return prepearePool(snapshot.val(), snapshot);
                    });
                },

                getLotteries: function() {
                    return lotteries.$loaded().then(function () {
                        var haveChanges = false;
                        angular.forEach(lotteries, function(value, key, obj) {
                            if (!value.id) {
                                value.id = key;
                                haveChanges = true;
                            }
                        });
                        
                        if (haveChanges) {
                            lotteries.$save();
                        }                        
                        
                        return lotteries;
                    });
                },

                createPool: function (pool) {
                    var ref;

                    if (!sessionService.getUserInfo().id) {
                        return $q.reject();
                    }

                    var userId = sessionService.getUserInfo().id;

                    if (!pool.id) {
                        ref = dbPools.push();

                        pool.id = ref.key;
                        pool.leaderId = userId;
                    } else {
                        ref = dbPools.child(pool.id);
                    }

                    pool.state = pool.state || '';
                    pool.members = {};
                    pool.members[userId] = {
                        balance: 0
                    };

                    pool._title = {};

                    var lotteriesToDelete = [];

                    _.forIn(pool.lotteries, function (value, key) {
                        if (!value) {
                            lotteriesToDelete.push(key);
                        }
                    });

                    _.each(lotteriesToDelete, function (key) {
                        delete pool.lotteries[key];
                    });

                    _.each(pool.title.toLowerCase().split(' '), function (word) {
                        var firstLetter = word[0];
                        pool._title[firstLetter] = word;
                    });

                    Object.keys(pool).forEach(function (key) {
                        if (typeof pool[key] === 'undefined'){
                            delete pool[key];
                        }
                    });

                    console.dir(pool);

                    return $q.all([
                        ref.set(pool),
                        sessionService.joinPool(pool.id)
                    ]).then(function(result) {
                        poolsService.getAllUserPools();

                        return result;
                    });
                }
            };

            return poolsService;
        }
    ]);
