var services = angular.module('poolMasterApp.services', []);

require('./session');
require('./pools');
require('./storage');
require('./friends');

module.exports = services;