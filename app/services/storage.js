var ImageTools = require('./../misc/imageTools.js');

angular.module('poolMasterApp.services')

    .factory('storageService', ['$q', 'sessionService',
        function ($q, sessionService) {
            var storageRef = firebase.storage().ref();
            function addNewImage(file) {
                if (!file) {
                    return $q.reject('file is needed');
                }

                return imageCrop(file).then(function(cropped) {
                    var metadata = {
                        'contentType': file.type
                    };

                    return storageRef.child('images/' + sessionService.getUserInfo().id + Date.now())
                        .put(cropped, metadata)
                        .then(function(snapshot) {
                            var url = snapshot.metadata.downloadURLs[0];
                            console.log('File available at', url);
                            return url;
                        }).catch(function(error) {
                            console.error('Upload failed:', error);
                        });
                });
            }

            function imageCrop(file) {
                var deferred = $q.defer();

                ImageTools.resize(file, {
                    width: 300, // maximum width
                    height: 300 // maximum height
                }, function(blob, didItResize) {
                    deferred.resolve(blob);
                });

                return deferred.promise;
            }

            return {
                addNewImage: addNewImage
            };
        }
    ]);
