require('angular-elastic');
require('./scss/ionic.app.scss');

var app = angular.module('poolMasterApp', [
    'ngSanitize',
    'ionic',
    'firebase',
    'monospaced.elastic',
    require('./services/services.module').name,
    require('./controllers/controllers.module').name,
    require('./directives/directives.module').name
]);

var config = {
    apiKey: "AIzaSyBZcjmYee67-WJRrKrLVKrkPyU6OJ2w5B4",
    authDomain: "project-4860348334848981735.firebaseapp.com",
    databaseURL: "https://project-4860348334848981735.firebaseio.com",
    storageBucket: "gs://project-4860348334848981735.appspot.com"
};
firebase.initializeApp(config);

app.run(function ($ionicPlatform, $rootScope, $state) {
    $rootScope.navigate = function (path, params, opts, specialFlags) {
        $rootScope.specialFlags = specialFlags;
        $state.go(path, params, opts);
    };
});

app.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        $stateProvider

        // setup an abstract state for the tabs directive
            .state('start', {
                url: '/start?userkey&action&poolId',
                template: require('./templates/start.html'),
                controller: 'StartCtrl',
                controllerAs: 'start',
                params: {
                    userkey: null,
                    action: null,
                    poolId: null
                }
            })

            .state('pools', {
                url: '/pools',
                template: require('./templates/pools.html'),
                controller: 'PoolsCtrl',
                controllerAs: 'pools'
            })

            .state('pool', {
                url: '',
                abstract: true,
                template: require('./templates/pool.html'),
                controller: 'PoolCtrl',
                controllerAs: 'pool',
                params: {
                    poolId: null
                }
            })

            .state('pool.name', {
                url: '/name',
                views: {
                    'poolsContent': {
                        template: require('./templates/pool-name.html')
                    }
                },
                params: {
                    edit: false
                }
            })

            .state('pool.type', {
                url: '/type',
                views: {
                    'poolsContent': {
                        template: require('./templates/pool-type.html')
                    }
                },
                params: {
                    edit: false
                }
            })

            .state('pool.description', {
                url: '/description',
                views: {
                    'poolsContent': {
                        template: require('./templates/pool-description.html')
                    }
                },
                params: {
                    edit: false
                }
            })

            .state('pool.location', {
                url: '/location',
                views: {
                    'poolsContent': {
                        template: require('./templates/pool-location.html')
                    }
                },
                params: {
                    edit: false,
                    fromLotteries: false
                }
            })

            .state('pool.lotteries', {
                url: '/lotteries',
                views: {
                    'poolsContent': {
                        template: require('./templates/pool-lotteries.html')
                    }
                },
                params: {
                    edit: false
                }
            })

            .state('pool.rules', {
                url: '/rules',
                views: {
                    'poolsContent': {
                        template: require('./templates/pool-rules.html')
                    }
                },
                params: {
                    edit: false
                }
            })

            .state('pool.tickets', {
                url: '/tickets',
                views: {
                    'poolsContent': {
                        template: require('./templates/pool-tickets.html')
                    }
                },
                params: {
                    edit: false
                }
            })

            .state('pool.money', {
                url: '/money',
                views: {
                    'poolsContent': {
                        template: require('./templates/pool-money.html')
                    }
                },
                params: {
                    edit: false
                }
            })

            .state('pool.review', {
                url: '/review',
                views: {
                    'poolsContent': {
                        template: require('./templates/pool-review.html')
                    }
                },
                params: {
                    poolId: null
                }
            })

            .state('pool.invite', {
                url: '/invite',
                views: {
                    'poolsContent': {
                        template: require('./templates/pool-invite.html')
                    }
                }
            })

            .state('pool.friends', {
                url: '/friends',
                views: {
                    'poolsContent': {
                        template: require('./templates/pool-friends.html')
                    }
                }
            });


        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/start');
    }
]);

module.exports = app;
