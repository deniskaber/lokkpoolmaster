var directives = angular.module('poolMasterApp.directives', [])

    .directive('focusMe', ['$timeout', function ($timeout) {
        return {
            link: function (scope, element, attrs) {
                scope.$on('viewLoaded', function () {
                    $timeout(function () {
                        element[0].focus();
                    }, 150);
                });
            }
        };
    }])

    .directive('focusMeImmediately', ['$timeout',
        function ($timeout) {
            return {
                link: function (scope, element, attrs) {
                    $timeout(function () {
                        element[0].focus();
                        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                            cordova.plugins.Keyboard.show(); //open keyboard manually
                        }
                    }, 150);
                }
            };
        }
    ])

    .directive('autoresize', ['$timeout',
        function ($timeout) {
            return {
                link: function (scope, element, attrs) {
                    function resize() {
                        element[0].style.height = 'auto';
                        element[0].style.height = element[0].scrollHeight + 'px';
                    }

                    element.on('change', resize);
                }
            };
        }
    ]);

module.exports = directives;